const { ApplicationError, InternalServerError } = require('../errors');

module.exports = (err, req, res, next) => {
   const e = err instanceof ApplicationError ? err : new InternalServerError();
   return res
     .status(e.statusCode)
     .json({ message: e.message });
};
