const authorization = 'authorization';
const delimiter = ' ';

const jwt = require('jsonwebtoken');
const { UnauthorizedError } = require('../errors');

module.exports = async (req, res, next) => {
  let [key, token] = req.headers[authorization] && req.headers[authorization].split(delimiter) || [,];
  token = token || key;

  if (!token) {
    throw new UnauthorizedError('Token is undefined');
  }

  try {
    req.user = jwt.verify(token, process.env.SECRET_KEY);
    next();
  } catch (error) {
    throw new UnauthorizedError('Unauthorized access to resourse');
  }
};

