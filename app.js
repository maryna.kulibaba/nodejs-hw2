require('dotenv').config();
require('express-async-errors');

const express = require('express');
const winston = require('winston');
const expressWinston = require('express-winston');
const cors = require('cors');
const connect = require('./services/connect');
const notFoundMiddleware = require('./middlewares/notFoundHandler');
const errorMiddleware = require('./middlewares/errorHandler');

const app = express();
app.use(express.static('./public'));

app.use(express.json());
app.use(cors({origin: '*'}));
app.use(expressWinston.logger({
  transports: [
    new winston.transports.File({ filename: 'all.log'}),
    new winston.transports.Console()
  ],
  format: winston.format.combine(
    winston.format.colorize(),
    winston.format.json()
  )
}));

app.use('/api/auth', require('./routes/auth'));
app.use('/api/users/me', require('./routes/user'));
app.use('/api/notes', require('./routes/note'));
app.use(notFoundMiddleware);
app.use(errorMiddleware);

const port = process.env.PORT || 8080;
const start = async () => {
  try {
    await connect(process.env.MONGO_URI);
    app.listen(port, console.log(`listening port: ${port}`));
  } catch (error) {
    console.log(error);
  }
};

start();
