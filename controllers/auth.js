const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { BadRequestError, NotFoundError } = require('../errors');
const { User } = require('../models');

const register = async (req, res, next) => {
  const { username, password } = req && req.body || {};

  if (!username || !password) {
    throw new BadRequestError("User name or password are not provided");
  }

  await (new User({
    username,
    password: await bcrypt.hash(password, 10)
  })).save();

  res.status(200).json({message: 'Success'});
}
  
const login = async (req, res, next) => {
  const {username, password} = req && req.body || {};

  if (!username || !password) {
    throw new BadRequestError("User name or password are not provided");
  }

  const user = await User.findOne({username});
  if (!user) {
    throw new BadRequestError("User not found"); // NotFoundError
  }

  if (!(await bcrypt.compare(password, user.password))) {
    throw new BadRequestError("Wrong password");
  }

  const jwt_token = jwt.sign({
      _id: user._id,
      username: user.username,
      password: user.password,
      createdDate: user.createdDate
    },
    process.env.SECRET_KEY
  );

  res.status(200).json({message: 'Succes', jwt_token});
}

module.exports = {
  register,
  login
}