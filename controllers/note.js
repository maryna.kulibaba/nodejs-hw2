const Offset = 0;
const Limit = 10;

const { BadRequestError } = require('../errors');
const { User } = require('../models');

const getAll = async (req, res) => {
  const {_id} = req.user;
  let offset = Number(req.query.offset) || Offset;
  let limit = Number(req.query.limit) || Limit;
  offset = offset >= 0 ? offset : Offset;
  limit = limit >= 0 ? limit : Limit;

  const user = await User.findOne({_id});
  const { notes } = user;
  let set = [];
  if (offset < notes.length ) {
    let indexOfEnd = offset + limit;
    indexOfEnd = indexOfEnd < notes.length ? indexOfEnd : notes.length;
    set = notes.slice(offset, indexOfEnd);
  }
  res.status(200).json({offset, limit, count: notes.length, notes: set});
 };

const create = async (req, res) => {
   const { _id } = req.user;
   const { text } = req && req.body || '';
   if (!text || text.trim().length === 0) {
     throw new BadRequestError('Note is empty');
   }

   const { notes } = await User.findOne({ _id });
   notes.push({ userId: _id, text });
   await User.findOneAndUpdate({_id}, {notes});
   res.status(200).json({message: 'Success'});
};

const get = async (req, res) => {
  const {_id} = req.user;
  const noteId = req.params.id;

  let note = await _find(_id, noteId);
  if (!note) {
    throw new NotFound('Note not found');
  }

  res.status(200).json({ note });
};

const edit = async (req, res) => {
  const { _id } = req.user;
  const noteId = req.params.id;
  const { text } = req && req.body || '';

  if (!text || text.length === 0) {
    throw new BadRequestError('Text is required');
  }

  let [notes, index] = await _find(_id, noteId);
  if (index === -1) {
    throw new BadRequestError('Note not found');
  }

  notes[index].text = text;
  await User.findOneAndUpdate({_id}, {notes});
  res.status(200).json({message: 'Success'});
};

const update = async (req, res) => {
  const { _id } = req.user;
  const noteId = req.params.id;

  let [notes, index] = await _find(_id, noteId);
  if (index === -1) {
    throw new BadRequestError('Note not found');
  }

  notes[index].completed = !notes[index].completed;
  await User.findOneAndUpdate({_id}, {notes});
  res.status(200).json({message: 'Success'});
};

const remove = async (req, res) => {
  const { _id } = req.user;
  const noteId = req.params.id;

  let [notes, index] = await _find(_id, noteId);
  if (index === -1) {
    throw new BadRequestError('Note not found');
  }

  notes.splice(index, 1);
  await User.findOneAndUpdate({_id}, {notes});
  res.status(200).json({message: 'Success'});
};

const _find = async (userId, noteId) => {
  const user = await User.findOne({ _id: userId });
  const { notes } = user;
  let index = notes.findIndex((n, i) => JSON.stringify(n._id) === JSON.stringify(noteId));
  return [notes, index];
};


module.exports = {
  getAll,
  create,
  get,
  edit,
  update,
  remove
};
