const bcrypt = require('bcrypt');
const { BadRequestError } = require('../errors');
const { User } = require('../models');

const get = (req, res) => {
  const { _id, username, createdDate } = req.user;
  res.status(200).json({ user: { _id, username, createdDate } });
};

const remove = async (req, res) => {
  await User.findOneAndRemove({ _id: req.user._id });
  res.status(200).json({ message: 'Success' });
};

const updatePassword = async (req, res) => {
  const { oldPassword, newPassword } = req && req.body || {};
  const { _id, password } = req.user;

  if (!(await bcrypt.compare(oldPassword, password)) ||
      await bcrypt.compare(newPassword, password)) {
    throw new BadRequestError();
  }
  await User.findOneAndUpdate({ _id }, { password: await bcrypt.hash(newPassword, 10) });
  res.status(200).json({ message: 'Success' });
};

module.exports = { 
  get,
  remove, 
  updatePassword 
};
