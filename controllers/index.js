const Auth = require('./auth');
const User = require('./user');
const Note = require('./note');

module.exports = {
  Auth,
  User,
  Note
};
