const ApplicationError = require('./applicationError');
const { StatusCodes } = require('http-status-codes');

class NotFoundError extends ApplicationError {
  constructor(message) {
    super(message);
    this.statusCode = StatusCodes.NOT_FOUND;
  }
}

module.exports = NotFoundError;