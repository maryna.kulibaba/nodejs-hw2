const ApplicationError = require('./applicationError');
const { StatusCodes } = require('http-status-codes');

class UnauthorizedError extends ApplicationError {
  constructor(message) {
    super(message);
    this.statusCode = StatusCodes.UNAUTHORIZED;
  }
}

module.exports = UnauthorizedError;