const ApplicationError = require('./applicationError');
const { StatusCodes } = require('http-status-codes');

class InternalServerError extends ApplicationError {
  constructor() {
    super('Internal server error, please try later or contact with support team');
    this.statusCode = StatusCodes.INTERNAL_SERVER_ERROR;
  }
}

module.exports = InternalServerError;