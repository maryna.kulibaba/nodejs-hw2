const ApplicationError = require('./applicationError');
const { StatusCodes } = require('http-status-codes');

class BadRequestError extends ApplicationError {
  constructor(message) {
    super(message);
    this.statusCode = StatusCodes.BAD_REQUEST;
  }
}

module.exports = BadRequestError;