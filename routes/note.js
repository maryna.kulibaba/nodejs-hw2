const express = require('express');
const router = express.Router();
const authMiddleware = require('../middlewares/authHandler');
const { Note } = require('../controllers')

router.use(authMiddleware);
router.route('/')
  .get(Note.getAll)
  .post(Note.create);
router.route('/:id')
  .get(Note.get)
  .put(Note.edit)
  .patch(Note.update)
  .delete(Note.remove);

module.exports = router;
