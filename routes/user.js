const express = require('express');
const router = express.Router();
const authMiddleware = require('../middlewares/authHandler');
const { User } = require('../controllers')

router.use(authMiddleware);
router.route('/')
    .get(User.get)
    .patch(User.updatePassword)
    .delete(User.remove);

module.exports = router;

