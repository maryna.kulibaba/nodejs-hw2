# RD LAB NODEJS HOMEWORK №2
Use NodeJS to implement CRUD API for notes.
***Last update** at 2022-04-15*

## Read
- [ ] [Project structure for an Express REST API when there is no "standard way"](https://www.coreycleary.me/project-structure-for-an-express-rest-api-when-there-is-no-standard-way)
- [ ] [Build a Node.js Express REST API with MongoDB & Atlas cloud service](https://medium.com/@ahmad.moussa/build-a-node-js-express-rest-api-with-mongodb-atlas-cloud-service-68711950ca43)
- [ ] [Error Handling in Express JS and express-async-errors package](https://medium.com/@utkuu/error-handling-in-express-js-and-express-async-errors-package-639c91ba3aa2)

## Useful tools
- [ ] [Markdown Editor](https://dillinger.io/) - an online cloud based HTML5 filled Markdown Editor
- [ ] [Visual Studio Code](https://code.visualstudio.com/) - a code editor redefined and optimized for building and debugging modern web and cloud applications.
- [ ] [Nodemon](https://nodemon.io/) - a utility depended on by over 1.5 million projects, that will monitor for any changes in your source and automatically restart your server

## Integrate with tools

- [ ] [ExpressJs](https://expressjs.com) - fast, unopinionated, minimalist web framework for Node.js
- [ ] [express-async-errors](https://github.com/davidbanham/express-async-errors#readme) - a very minimalistic and unintrusive hack, instead of patching all methods on an express Router (wrap they in try-catch block), it wraps the Layer#handle property in one place, leaving all the rest of the express guts intact
- [ ] [express-validator](https://express-validator.github.io/docs/) - a set of express.js middlewares that wraps validator.js validator and sanitizer functions.
- [ ] [http-status-codes](https://github.com/prettymuchbryce/http-status-codes#readme) - constants enumerating the HTTP status codes
- [ ] [bcrypt](https://github.com/kelektiv/node.bcrypt.js#readme) - a library to help you hash passwords
- [ ] [jsonwebtoken](https://github.com/auth0/node-jsonwebtoken#readme) - JSON Web Token implementation (symmetric and asymmetric)
- [ ] [winston](https://github.com/winstonjs/winston#readme) - a logger for just about everything
- [ ] [express-winston](https://github.com/bithavoc/express-winston#readme) - a middleware to log your HTTP requests with Winston logger
- [ ] [mongoose](https://mongoosejs.com/) - is a MongoDB object modeling tool designed to work in an asynchronous environment
- [ ] [dotenv](https://github.com/motdotla/dotenv#readme) - a zero-dependency module that loads environment variables from a .env file into process.env
- [ ] [cors](https://github.com/expressjs/cors#readme) - a node.js package for providing a Connect/Express middleware that can be used to enable CORS with various options
- [ ] [eslint](https://eslint.org/) - statically analyzes your code to quickly find problems
- [ ] [eslint-config-google](https://github.com/google/eslint-config-google#readme) - ESLint shareable config for the Google JavaScript style guide (ES2015+ version)

## Structure

- **controllers** - take request object, pull out data from request, validate, then send to service(s)
- **errors** - application errors
- **middlewares** - functions that execute during the lifecycle of a request to the Express server
- **models** - part of an ORM
- **routes** - handle the HTTP requests that hits the API and route them to appropriate controller(s)
- **services** - Contains the business logic, derived from business and technical requirements, as well as how we access our data stores
- .env - app configuration
- app.js - server

***
